<?php
/**
 * Copyright © 2016 Mindit. All rights reserved.
 * See COPYING.txt for license details.
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Mindit_CommandExample',
    __DIR__
);
